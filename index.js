function ketQua() {
    var diemChuan = document.getElementById("diemchuan").value * 1;
    var diemKhuVuc = document.getElementById("khuvuc").value * 1;
    var diemDoiTuong = document.getElementById("doituong").value * 1;
    var diemMon1 = document.getElementById("diem1").value * 1;
    var diemMon2 = document.getElementById("diem2").value * 1;
    var diemMon3 = document.getElementById("diem3").value * 1;
    var result;
    var tongDiem = diemMon1 + diemMon2 + diemMon3 + diemKhuVuc + diemDoiTuong;
    if (diemMon1 <= 0 || diemMon2 <= 0 || diemMon3 <= 0) {
        result = `👉 Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0.`;
    } else if (tongDiem >= diemChuan) {
        result = `👉 Bạn đã đậu. Tổng điểm của bạn là: ${tongDiem}.`;
    } else {
        result = `👉Bạn đã rớt. Tổng điểm: ${tongDiem}.`;
    }
    document.getElementById("result").innerHTML = result;
}
// bt 2
function tinhTien(hoten, sokw) {
    var result;
    var soTien;
    if (sokw <= 50) {
        soTien = 50 * sokw;
    } else if (sokw <= 100) {
        soTien = 500 * 50 + (sokw - 50) * 650;
    } else if (sokw <= 200) {
        soTien = 500 * 50 + 50 * 650 + (sokw - 100) * 850;
    } else if (sokw <= 350) {
        soTien = 500 * 50 + 50 * 650 + 100 * 850 + (sokw - 200) * 1100;
    } else {
        soTien =
            500 * 50 + 50 * 650 + 100 * 850 + 150 * 1100 + (sokw - 350) * 1300;
    }
    soTien = new Intl.NumberFormat("vn-VN").format(soTien);
    result = `👉Họ tên: ${hoten}; Tiền điện: ${soTien}`;
    return result;
}
function tinhTienDien() {
    var hoTen = document.getElementById("hoten").value;
    var soKw = document.getElementById("sokw").value * 1;
    var result = tinhTien(hoTen, soKw);
    document.getElementById("result2").innerHTML = result;
}
// bt3
function tinhTienThue() {
    var hoTen = document.getElementById("hoten3").value;
    var thuNhapNam = document.getElementById("thunhap").value * 1;
    var phuThuoc = document.getElementById("phuthuoc").value * 1;
    var thuNhapChiuThue;
    if (thuNhapNam <= 60000000) {
        thuNhapChiuThue = (thuNhapNam - 4000000 - phuThuoc * 1600000) * 0.05;
    } else if (thuNhapNam <= 120000000) {
        thuNhapChiuThue = (thuNhapNam - 4000000 - phuThuoc * 1600000) * 0.1;
    } else if (thuNhapNam <= 210000000) {
        thuNhapChiuThue = (thuNhapNam - 4000000 - phuThuoc * 1600000) * 0.15;
    } else if (thuNhapNam <= 384000000) {
        thuNhapChiuThue = (thuNhapNam - 4000000 - phuThuoc * 1600000) * 0.2;
    } else if (thuNhapNam <= 624000000) {
        thuNhapChiuThue = (thuNhapNam - 4000000 - phuThuoc * 1600000) * 0.25;
    } else if (thuNhapNam <= 960000000) {
        thuNhapChiuThue = (thuNhapNam - 4000000 - phuThuoc * 1600000) * 0.3;
    } else {
        thuNhapChiuThue = (thuNhapNam - 4000000 - phuThuoc * 1600000) * 0.35;
    }
    thuNhapChiuThue = new Intl.NumberFormat("vn-VN").format(thuNhapChiuThue);

    var result = `👉Họ tên: ${hoTen}; Tiền thuế thu nhập cá nhân: ${thuNhapChiuThue} VND`;
    document.getElementById("result3").innerHTML = result;
}
// bt4
function appear() {
    var khachhang = document.getElementById("loaikhachhang").value;
    // if (a == 1.5) document.getElementById("disappear").style.display = "block";
    // else document.getElementById("disappear").style.display = "none";
    if (khachhang == "doanhnghiep") {
        document.getElementById("disappear").classList.remove("d-none");
    } else document.getElementById("disappear").classList.add("d-none");
}
function tinhTienNhaDan(soKenh) {
    var tienDichVu;
    tienDichVu = 4.5 + 20.5 + 7.5 * soKenh;
    return tienDichVu;
}
function tinhTienDoanhNghiep(soKenh, soKetNoi) {
    var tienDichVu;
    if (soKetNoi <= 10) {
        tienDichVu = 15 + 75 + soKenh * 50;
    } else {
        tienDichVu = 15 + 75 + soKenh * 50 + (soKetNoi - 10) * 5;
    }
    return tienDichVu;
}
function tinhTienCap() {
    var tongTien;
    var maKhachHang = document.getElementById("makhachhang").value;
    var soKenh = document.getElementById("sokenh").value * 1;
    var soKetNoi = document.getElementById("soketnoi").value * 1;
    var loaiKhachHang = document.getElementById("loaikhachhang").value;
    if (loaiKhachHang == "nhadan") tongTien = tinhTienNhaDan(soKenh);
    else if (loaiKhachHang == "doanhnghiep")
        tongTien = tinhTienDoanhNghiep(soKenh, soKetNoi);
    tongTien = new Intl.NumberFormat("en", {
        style: "currency",
        currency: "USD",
    }).format(tongTien);
    var result = `👉Mã khách hàng: ${maKhachHang}; Tiền cáp: ${tongTien}`;
    document.getElementById("result4").innerHTML = result;
}
